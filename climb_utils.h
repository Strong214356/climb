#ifndef CLIMB_UTILS_H
#define CLIMB_UTILS_H

#include "toml.h"

void usage(const char *prog, const char *reason);
char *join(const char* s1, const char* s2);
toml_table_t *getConfig();
bool exist(char *path);
void printLoadBar(float percent);
bool dl_file(char *url, char *file);

#endif // CLIMB_UTILS_H
