#include "climb_utils.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>

bool exist(char *path) {
        return access(path, F_OK) == 0;
}

static bool check(char *path) {
        bool ret = false;
        size_t len = strlen(path);
        for(size_t i = 0; i < len; i++) {
                ret = true;
                if(!(isalnum(path[i]) || path[i] == '_')) return false;
        }
        return ret;
}

bool writeConfig(char *path, char *type) {
        FILE *config = fopen("climb.toml", "w");
        if(!config) return false;
        fprintf(config, "[project]\n");
        fprintf(config, "    name = \"%s\"\n", path);
        fprintf(config, "    version = \"1.0\"\n");
        fprintf(config, "    cc = \"gcc\"\n");
        fprintf(config, "[%s]\n", type);
        fprintf(config, "    name = \"%s\"\n", path);
        fprintf(config, "    flags = [\"-Iinc\", \"-Wall\", \"-Wextra\"]\n");
        fprintf(config, "    dflags = [\"-g\", \"-fsanitize=address\"]\n");
        fprintf(config, "[release]\n");
        fprintf(config, "    build = \"stable\"\n");
        fprintf(config, "    script = \"\"\n");
        fclose(config);
        return true;
}

bool makeStructure(char *path) {
        if(mkdir(path, 0755) != 0) return false;
        if(chdir(path) != 0) return false;
        if(mkdir("src", 0755) != 0) return false;
        if(mkdir("inc", 0755) != 0) return false;
        FILE *m = fopen("src/main.c", "w");
        if(!m) return false;
        fprintf(m, "#include <stdlib.h>\n");
        fprintf(m, "#include <stdio.h>\n\n");
        fprintf(m, "int main() {\n");
        fprintf(m, "        printf(\"Hello %s!\\n\");\n", path);
        fprintf(m, "        return EXIT_SUCCESS;\n");
        fprintf(m, "}");
        fclose(m);
        return true;
}

int init(int c, char **v) {
        if(c < 2) {
                usage(v[-1], "not enough argument");
                return EXIT_FAILURE;
        }
        if(c > 3) {
                usage(v[-1], "too much argument");
                return EXIT_FAILURE;
        }
        char *type = "executable";
        char *path = v[1];
        if(c == 3) type = v[2];
        if(!check(path)) {
                printf("[ERROR] => project name need to match this expression : '[a-zA-Z0-9_]+'");
                return EXIT_FAILURE;
        }
        if(exist(path)) {
                printf("[ERROR] => directory %s already exit\n", v[1]);
                return EXIT_FAILURE;
        }
        if(!makeStructure(path)) {
                printf("[ERROR] => unable to setup file structure\n");
                return EXIT_FAILURE;
        }
        if(!writeConfig(path, type)) {
                printf("[ERROR] => unable to setup file structure\n");
                return EXIT_FAILURE;
        }
        printf("[INFO] => project %s initialized successfully\n", path);
        return EXIT_SUCCESS;
}
