#include "climb_utils.h"
#include "climb_cmd.h"
#include <stdlib.h>
#include <stdio.h>

int list(int c, char **v) {
        if(c > 1) {
                usage(v[-1], "too much arguments");
                return EXIT_FAILURE;
        }
        toml_table_t *cfg = getConfig();
        if(!cfg) return EXIT_FAILURE;
        toml_table_t *exec = toml_table_table(cfg, "executable");
        if(!exec) {
                printf("[ERROR] => corrupted config file: unable to find executable table\n");
                toml_free(cfg);
                return EXIT_FAILURE;
        }
        toml_value_t vl = toml_table_string(exec, "name");
        if(!vl.ok) {
                printf("[ERROR] => corrupted config file: unable to find executable table\n");
                toml_free(cfg);
                return EXIT_FAILURE;
        }
        printf("[INFO] => Libraries for executable '%.*s'\n", vl.u.sl, vl.u.s);
        toml_array_t *libs = toml_table_array(exec, "libs");
        if(!libs) {
                printf("[ERROR] => corrupted config file: unable to find executable table\n");
                toml_free(cfg);
                return EXIT_FAILURE;
        }
        int lib_num = toml_array_len(libs);
        for(int i = 0; i < lib_num; i++) {
                toml_value_t val = toml_array_string(libs, i);
                if(!val.ok) fprintf(stderr, "[WARNING] => unable to get library at index %d\n", i);
                else printf("\t- %.*s;\n", val.u.sl, val.u.s);
        }
        toml_free(cfg);
        return EXIT_SUCCESS;
}
