#include "climb_utils.h"
#include "climb_cmd.h"
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "macros.h"
#include <stdio.h>
#include "climb.h"

void usage(const char *prog, const char *reason) {
        printf("Usage : %s <command> [OPTION]\n", prog);
        printf("commands :\n");
        for(int i = 0; i < ARR_LEN(COMMANDS); i++) {
                printf("\t%s -- %s\n", COMMANDS[i].name, COMMANDS[i].desc);
        }
        printf("\n[INFO] => %s\n", reason);
}

int help(int c, char **v) {
        if(c > 2) {
                usage(v[-1], "too much arguments");
                return EXIT_FAILURE;
        }
        if(c < 2) {
                usage(v[-1], "hope this helped");
                return EXIT_SUCCESS;
        }
        bool founded = false;
        for(int i = 0; i < ARR_LEN(COMMANDS); i++) {
                if(strcmp(COMMANDS[i].name, v[1]) == 0) {
                        founded = true;
                        printf("Help of the %s command:\n%s\n", COMMANDS[i].name, COMMANDS[i].help);
                        break;
                }
        }
        if(founded) return EXIT_SUCCESS;
        else {
                usage(v[-1], "unknown command");
                return EXIT_FAILURE;
        }
}
