#include "climb_utils.h"
#include "climb_cmd.h"
#define NOB_IMPLEMENTATION
#include "nob.h"

void build_append_files(Nob_Cmd *cmd) {
        DIR *srcs;
        struct dirent *ent;
        if((srcs = opendir("src"))) {
                while((ent = readdir(srcs))) {
                        if(ent->d_type != DT_REG)
                                continue;
                        if(ent->d_name[0] == '.')
                                continue;
                        // TODO: remove this and replace it by a better system
                        nob_cmd_append(cmd, join("src/", ent->d_name));
                }
                closedir(srcs);
                return;
        }
        printf("[ERROR] => unable to open source directory\n");
        exit(1);
}

int build(int c, char **v) {
        if(c > 1) {
                usage(v[-1], "too much arguments");
                return EXIT_FAILURE;
        }
        toml_table_t *config = getConfig();
        if(!config)
                return EXIT_FAILURE;
        toml_table_t *project = toml_table_table(config, "project");
        if(!project) {
                printf("[ERROR] => corrupted config file: unable to find project table\n");
                toml_free(config);
                return EXIT_FAILURE;
        }
        toml_table_t *executable = toml_table_table(config, "executable");
        if(!executable) {
                printf("[ERROR] => corrupted config file: unable to find executable table\n");
                toml_free(config);
                return EXIT_FAILURE;
        }
        if(!exist("build"))
                mkdir("build", 0755);
        if(!exist("build/release"))
                mkdir("build/release", 0755);
        Nob_Cmd cmd = {0};
        toml_value_t cc = toml_table_string(project, "cc");
        if(!cc.ok)
                cc.u.s = "gcc";
        toml_value_t name = toml_table_string(executable, "name");
        if(!name.ok) {
                printf("[ERROR] => corrupted config file: no executable name\n");
                toml_free(config);
                return EXIT_FAILURE;
        }
        toml_array_t *flags = toml_table_array(executable, "flags");
        if(!flags) {
                printf("[ERROR] => corrupted config file: no compilation flags\n");
                toml_free(config);
                return EXIT_FAILURE;
        }
        char *exe = join("build/release/", name.u.s);
        nob_cmd_append(&cmd, cc.u.s, "-o", exe);
        int flagL = toml_array_len(flags);
        for(int i = 0; i < flagL; i++) {
                toml_value_t f = toml_array_string(flags, i);
                if(!f.ok)
                        printf("[WARNING] => unable to get flags at index %d, skipping...\n", i);
                else
                        nob_cmd_append(&cmd, f.u.s);
        }
        build_append_files(&cmd);
        // TODO : add libraries
        int ret = EXIT_SUCCESS;
        if(!nob_cmd_run_sync(cmd)) {
                printf("[ERROR] => unable to compile project %s\n", name.u.s);
                ret = EXIT_FAILURE;
        }
        nob_cmd_free(cmd);
        free(exe);
        toml_free(config);
        return ret;
}
