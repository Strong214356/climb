#include<sys/socket.h>
#include <arpa/inet.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <regex.h>
#include <netdb.h>
#include <stdio.h>

#include "https.h"

#define CHECK(x) assert((x) >= 0)
#define LOOP_CHECK(rval, cmd)                                             \
        do {                                                              \
                rval = cmd;                                               \
        } while (rval == GNUTLS_E_AGAIN || rval == GNUTLS_E_INTERRUPTED); \
        assert(rval >= 0)

#ifndef HTTP_TIME_OUT
#define HTTP_TIME_OUT 360
#endif

#ifndef HTTP_USER_AGENT
#define HTTP_USER_AGENT "Climb/1.0 (Linux)"
#endif

/* low level methods */

struct http_url *https_parse_url(const char *url) {
        struct http_url *hu;
        regmatch_t match[6];
        regex_t re;
        char *buf;
        if (!url || strlen(url) < 1 || !(hu = calloc(1, sizeof(struct http_url) + strlen(url) + 1))) return NULL;
        buf = (char *) hu + sizeof(struct http_url);
        memcpy(buf, url, strlen(url));

        if(regcomp(&re, "^(https?)[:]\\/{2}(([^\\/.]+\\.?){2,})((\\/[^\\/]+)+\\/?)$", REG_EXTENDED) != 0) {
                free(hu);
                return NULL;
        }
        if(regexec(&re, buf, 6, match, 0) == REG_NOMATCH) {
                https_err = "Bad url";
                free(hu);
                return NULL;
        }
        hu->protocol = buf;
        hu->host = buf + match[2].rm_so;
        hu->host[-1] = 0;
        hu->host[-3] = 0;
        hu->query = buf + match[4].rm_so + 1;
        hu->query[-1] = 0;

        return hu;
}

gnutls_certificate_credentials_t init_ssl() {
        gnutls_certificate_credentials_t xcred;
        CHECK(gnutls_global_init());
        CHECK(gnutls_certificate_allocate_credentials(&xcred));
        CHECK(gnutls_certificate_set_x509_system_trust(xcred));
        return xcred;
}

void free_ssl(gnutls_session_t session, gnutls_certificate_credentials_t xcred, int sock) {
        close(sock);
        gnutls_deinit(session);
        gnutls_certificate_free_credentials(xcred);
        gnutls_global_deinit();
}

int http_connect(struct http_url *hu) {
        int sockfd = -1;
        struct addrinfo hints = {0};
        struct addrinfo *result = NULL;
        struct addrinfo *rr;
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_STREAM;
        hints.ai_flags = AI_ADDRCONFIG | AI_NUMERICSERV;
        hints.ai_protocol = IPPROTO_TCP;
        if (getaddrinfo(hu->host, "443", &hints, &result) != 0 || result == NULL) exit(EXIT_FAILURE);
        for (rr = result; rr != NULL; rr = rr->ai_next) {
                sockfd = socket(rr->ai_family, rr->ai_socktype, rr->ai_protocol);
                if (sockfd == -1) continue;
                if (connect(sockfd, rr->ai_addr, rr->ai_addrlen) != -1) break;
                close(sockfd);
        }
        freeaddrinfo(result);
        return sockfd;
}

int https_connect(struct http_url *hu, gnutls_session_t* session, gnutls_certificate_credentials_t xcred) {
        int sd = http_connect(hu);
        int bytes = 0;
        CHECK(gnutls_init(session, GNUTLS_CLIENT));

        CHECK(gnutls_server_name_set(*session, GNUTLS_NAME_DNS,
                                hu->host,
                                strlen(hu->host)));

        CHECK(gnutls_set_default_priority(*session));

        CHECK(gnutls_credentials_set(*session, GNUTLS_CRD_CERTIFICATE, xcred));
        gnutls_session_set_verify_cert(*session, hu->host, 0);

        gnutls_transport_set_int(*session, sd);
        gnutls_handshake_set_timeout(*session, GNUTLS_DEFAULT_HANDSHAKE_TIMEOUT);
        do {
                bytes = gnutls_handshake(*session);
        } while (bytes < 0 && gnutls_error_is_fatal(bytes) == 0);
        if(bytes < 0) {
                fprintf(stderr, "*** Handshake failed: %s\n", gnutls_strerror(bytes));
                exit(1);
        }
        return sd;
}

int http_send(int fd, const char *rq) {
        if (!rq) return -1;
        int bytes = write(fd, rq, strlen(rq));
        if (bytes < 0) return -1;
        return 0;
}

int https_send(gnutls_session_t session, const char *rq) {
        if (!rq) return -1;
        int bytes;
        LOOP_CHECK(bytes, gnutls_record_send(session, rq, strlen(rq)));
        return 0;
}

static void http_cut_trailing_crlf(struct http_message *msg) {
        if (msg->state.chunk == 1 && msg->length > 0) {
                msg->content[--msg->length] = 0;
        } else if (!msg->state.chunk && msg->length > 1) {
                msg->length -= 2;
                msg->content[msg->length] = 0;
        }
}

static char *http_parse_content(struct http_message *msg, char *bod, char *eod) {
        int len = eod - bod;

        if (msg->state.chunk > -1) {
                if (msg->state.chunk < len) {
                        char *eoc;
                        if (msg->state.chunk > 0) {
                                if (msg->state.chunk == 1) {
                                        ++bod;
                                        msg->length = 0;
                                        msg->state.chunk = 0;
                                } else {
                                        msg->length = msg->state.chunk;
                                        msg->content = bod;
                                        bod += msg->state.chunk;
                                        msg->state.chunk = 0;
                                        http_cut_trailing_crlf(msg);
                                }
                                return bod;
                        }
                        if (!(eoc = strchr(bod, '\n'))) {
                                return bod;
                        }
                        *eoc = 0;
                        sscanf(bod, "%x", &msg->state.chunk);
                        msg->state.chunk += 2;
                        msg->content = ++eoc;
                        len = eod - msg->content;
                        if (msg->state.chunk < len) {
                                msg->length = msg->state.chunk;
                                bod = msg->content + msg->state.chunk;
                                msg->state.chunk = 0;
                                http_cut_trailing_crlf(msg);
                                return bod;
                        } else {
                                msg->length = len;
                                msg->state.chunk -= len;
                                http_cut_trailing_crlf(msg);
                                return eod;
                        }
                } else {
                        msg->state.chunk -= len;
                }
        }
        msg->content = bod;
        msg->length = len;
        http_cut_trailing_crlf(msg);
        return eod;
}

static char *http_parse_message(
                struct http_message *msg,
                char *bod,
                char *eod) {
        char *lf;

        if (!bod) {
                return bod;
        }

        if (msg->state.in_content) {
                return http_parse_content(msg, bod, eod);
        }

        /* header line is incomplete so fetch more data */
        if (!(lf = strchr(bod, '\n'))) {
                return bod;
        }

        /* parse status code */
        if (!msg->header.code) {
                /* accept HTTP/1.[01] only */
                if (strncmp(bod, "HTTP/1.", 7) ||
                                !strchr("01", *(bod + 7))) {
                        return NULL;
                }

                bod += 8;
                msg->header.code = atoi(bod);

                return http_parse_message(msg, ++lf, eod);
        }

        /* parse header line by line */
        for (; lf; bod = ++lf, lf = strchr(bod, '\n')) {
#define WHITESPACE " \t\r\n"
                char *value;

                *lf = 0;

                /* check for end of header */
                if (!*bod || !strcmp(bod, "\r")) {
                        if (!*bod) {
                                ++bod;
                        } else {
                                bod += 2;
                        }

                        msg->state.in_content = 1;

                        return http_parse_message(msg, bod, eod);
                }

                if (!(value = strchr(bod, ':'))) {
                        continue;
                }

                *value = 0;
                ++value;

                bod += strspn(bod, WHITESPACE);
                strtok(bod, WHITESPACE);

                value += strspn(value, WHITESPACE);
                strtok(value, WHITESPACE);

                if (!strcasecmp(bod, "Transfer-Encoding") &&
                                !strcasecmp(value, "chunked")) {
                        /* means 0 bytes until next chunk header */
                        msg->state.chunk = 0;
                } else if (!strcasecmp(bod, "Content-Length")) {
                        msg->header.length = atoi(value);
                }
        }

        return bod;
}

int https_read(gnutls_session_t session, struct http_message *msg) {
        if (!msg) {
                return -1;
        }

        if (!msg->state.offset) {
                msg->state.size = sizeof(msg->state.buf) - 1;
                msg->state.offset = msg->state.buf;
                msg->state.chunk = -1;
                msg->header.length = -1;
        }

        if (msg->state.total == msg->header.length) {
                return 0;
        }

        for (;;) {
                if (msg->state.left > 0) {
                        char *parsed_until;

                        msg->length = 0;

                        parsed_until = http_parse_message(
                                        msg,
                                        msg->state.offset,
                                        msg->state.offset + msg->state.left);

                        if (parsed_until > msg->state.offset) {
                                msg->state.left -= parsed_until - msg->state.offset;
                                msg->state.offset = parsed_until;
                                msg->state.total += msg->length;
                                return 1;
                        }
                }
                if (msg->state.offset > msg->state.buf) {
                        if (msg->state.left > 0)
                                memmove(
                                                msg->state.buf,
                                                msg->state.offset,
                                                msg->state.left);

                        msg->state.offset = msg->state.buf;
                }
                {
                        char *append = msg->state.offset + msg->state.left;
                        int bytes,
                            size = (msg->state.buf + msg->state.size)-append;
                        if (size < 1) {
                                int half = msg->state.size >> 1;

                                memcpy(
                                                msg->state.buf,
                                                msg->state.buf + half,
                                                half);

                                msg->state.offset = msg->state.buf;
                                msg->state.left = half;

                                append = msg->state.offset + msg->state.left;
                                size = msg->state.size - msg->state.left;
                        }
                        LOOP_CHECK(bytes, gnutls_record_recv(session, append, size));
                        if (bytes < 0) {
                                https_err = "error on file read";
                                return -1;
                        }
                        if (bytes == 0) {
                                return 0;
                        }

                        append[bytes] = 0;
                        msg->state.left += bytes;
                }
        }

        return 0;
}

char *https_err = NULL;
int https_code = 200;

char *https_get_code() {
        switch(https_code) {
                case 200:
                        return "Success";
                case 400:
                        return "Bad Request";
                case 403:
                        return "Forbidden";
                case 404:
                        return "Not Found";
                case 418:
                        return "I am a teapot";
                default:
                        return "WIP";
        }
}

bool https_download_file(char *url, char *file) {
        https_code = 200;
        https_err = NULL;
        bool ret = false;
        int sd = -1;
        FILE *f = fopen(file, "w");
        if(!f) {
                https_err = "unable to open file";
                goto err;
        }
        gnutls_certificate_credentials_t xcred = init_ssl();
        gnutls_session_t session;
        struct http_url *hu = https_parse_url(url);
        sd = https_connect(hu, &session, xcred);
        if(https_send(session, "GET /")
        || https_send(session, hu->query)
        || https_send(session, " HTTP/1.1\r\nHost: ")
        || https_send(session, hu->host)
        || https_send(session, "\r\n"
                           "User-Agent: " HTTP_USER_AGENT "\r\n"
                           "Accept: */*\r\n"
                           "Connection: close\r\n\r\n")) goto conerr;
        struct http_message msg = {0};
        while(https_read(session, &msg) > 0) if(msg.content) fprintf(f, "%.*s", msg.length, msg.content);
        if(msg.header.code != 200) {
                https_code = msg.header.code;
                https_err = "Error code recieved";
                goto conerr;
        }
        ret = true;
conerr:
        free_ssl(session, xcred, sd);
        free(hu);
        fclose(f);
err:
        return ret;
}

struct https_handler https_get_download_handler(char *url) {
        https_code = 200;
        https_err = NULL;
        struct https_handler ret = {0};
        ret.fd = -1;
        ret.cred = init_ssl();
        struct http_url *hu = https_parse_url(url);
        ret.fd = https_connect(hu, &ret.session, ret.cred);
        if(https_send(ret.session, "GET /")
        || https_send(ret.session, hu->query)
        || https_send(ret.session, " HTTP/1.1\r\nHost: ")
        || https_send(ret.session, hu->host)
        || https_send(ret.session, "\r\n"
                           "User-Agent: " HTTP_USER_AGENT "\r\n"
                           "Accept: */*\r\n"
                           "Connection: close\r\n\r\n")) goto urlerr;
        ret.len = 0;
        ret.setlen = true;
        ret.valid = true;
urlerr:
        free(hu);
        return ret;
}

struct http_string https_read_partial(struct https_handler *hand, bool *valid, bool *end) {
        https_code = 200;
        *end = false;
        *valid = true;
        hand->msg.header.code = 200;
        int bytes = https_read(hand->session, &hand->msg);
        if(bytes < 0) {
                *valid = false;
                return (struct http_string){0};
        }
        if(bytes == 0) *end = true;
        if(hand->len == 0 && hand->setlen) {
                hand->len = hand->msg.header.length;
                hand->setlen = false;
        }
        struct http_string str = {0};
        str.len = hand->msg.length;
        if(hand->msg.content) memcpy(str.buf, hand->msg.content, str.len * sizeof(char));
        if(hand->msg.header.code != 200) {
                *valid = false;
                https_code = hand->msg.header.code;
                https_err = "Error code recieved";
                return (struct http_string){0};
        }
        return str;
}

void https_destroy_handler(struct https_handler *hand) {
        free_ssl(hand->session, hand->cred, hand->fd);
}
