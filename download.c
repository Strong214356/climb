/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   download.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tdelage <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2024/05/15 08:23:02 by tdelage           #+#    #+#             */
/*   Updated: 2024/05/22 02:30:03 by tdelage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "climb_utils.h"
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include "https.h"
#include <math.h>
#include <sys/time.h>
#define S2U 1000000

int numChars(int i) {
        int j = 0;
        do {
                i /= 10;
                j++;
        } while(i);
        return j;
}

int sizeTps(int tps) {
        while(tps > 1024) tps /= 1000;
        return numChars(tps);
}

char *Tps_units[] = {" B/s", "Kb/s", "Mb/s", "Gb/s"};

void printBBS(tps) {
        int i = 0;
        while(tps > 1024) {
                tps /= 1000;
                i++;
        }
        printf("%d", tps);
        printf(" %s", Tps_units[i]);
}

void printLoadBar(float percent) {
        struct winsize win;
        ioctl(STDOUT_FILENO, TIOCGWINSZ, &win);
        int cols;
        if(win.ws_col <= 0) cols = 100 - (14 + numChars((int)roundf(percent * 100)));
        else cols = win.ws_col - (14 + numChars((int)roundf(percent * 100)));
        int len = roundf(percent * cols);
        printf("\r[");
        for(int i = 0; i < len - 1; i++) {
                printf("-");
        }
        if((int)roundf(percent * 100) != 0) printf("#");
        for(int i = 0; i < cols - len; i++) {
                printf(" ");
        }
        printf("] %d%% ", (int)roundf(percent * 100));
        fflush(stdout);
}

bool dl_file(char *url, char *file) {
        FILE *f = fopen(file, "w");
        struct https_handler h = https_get_download_handler(url);
        if(!h.valid) {
                https_destroy_handler(&h);
                fclose(f);
                return false;
        }
        bool end = false;
        bool valid = false;
        struct http_string str = {0};
        int len = 0;
        do {
                if(h.len) printLoadBar((float)len/h.len);
                if(valid) fprintf(f, "%.*s", str.len, str.buf);
                str = https_read_partial(&h, &valid, &end);
                if(!valid) {
                        https_destroy_handler(&h);
                        fclose(f);
                        return false;
                }
                len += str.len;
        } while(!end);
        https_destroy_handler(&h);
        fclose(f);
        printLoadBar(1);
        printf("\n");
        return true;
}

FILE *getRepoList(char *url, char *repo_name) {
        char *name = join("/tmp/Climb_repo_", repo_name);
        if(!name) return NULL;
        if(exist(name)) {
                FILE *ret = fopen(name, "r");
                free(name);
                return ret;
        }
        if(!https_download_file(url, name)) {
                if(https_code != 200) printf("[ERROR] => Error %d %s\n", https_code, https_get_code());
                else printf("[ERROR] => %s\n", https_err);
                return NULL;
        }
        FILE *f = fopen(name, "r");
        https_download_file(url, name);
        free(name);
        return f;
}
