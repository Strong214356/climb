#ifndef CLIMB_H
#define CLIMB_H

typedef int (*cmdCallback)(int, char **);

struct Command {
        char *name;
        char *desc;
        char *help;
        cmdCallback callback;
};

extern struct Command COMMANDS[6];

#endif // CLIMB_H
