#include "climb_utils.h"
#include "climb_cmd.h"
#include "nob.h"

int run(int c, char **v) {
        if(build(c, v) == EXIT_FAILURE)
                return EXIT_FAILURE;
        toml_table_t *config = getConfig();
        if(!config)
                return EXIT_FAILURE;
        toml_table_t *executable = toml_table_table(config, "executable");
        if(!executable) {
                printf("[ERROR] => unable to run project: libraries are not executables\n");
                toml_free(config);
                return EXIT_FAILURE;
        }
        toml_value_t name = toml_table_string(executable, "name");
        if(!name.ok) {
                printf("[ERROR] => corrupted config file: no executable name\n");
                toml_free(config);
                return EXIT_FAILURE;
        }
        toml_array_t *flags = toml_table_array(executable, "eflags");
        if(!flags) {
                printf("[ERROR] => corrupted config file: no execution flags\n");
                toml_free(config);
                return EXIT_FAILURE;
        }
        char *exec = join("./build/release/", name.u.s);
        printf("-------- running %s\n\n", exec);
        Nob_Cmd cmd = {0};
        nob_cmd_append(&cmd, exec);
        int flagL = toml_array_len(flags);
        for(int i = 0; i < flagL; i++) {
                toml_value_t f = toml_array_string(flags, i);
                if(!f.ok)
                        printf("[WARNING] => unable to get flags at index %d, skipping...\n", i);
                else
                        nob_cmd_append(&cmd, f.u.s);
        }
        int ret = EXIT_SUCCESS;
        if(!nob_cmd_run_sync(cmd)) {
                printf("[ERROR] => project execution did not exited with 0 exit code\n");
                ret = EXIT_FAILURE;
        }
        nob_cmd_free(cmd);
        free(exec);
        toml_free(config);
        return ret;
}
