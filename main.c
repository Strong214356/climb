#include "climb_utils.h"
#include "climb_cmd.h"
#include <stdlib.h>
#include <string.h>
#include "macros.h"
#include "climb.h"
#include <unistd.h>
#include "https.h"

struct Command COMMANDS[6] = {
        [0] = (struct Command) {
                .name = "help",
                .desc = "show help (use help <command> for specific help)",
                .help =
"\
        the help command is here to help you to use this program.\n\
        you can use the command as help to get global help\n\
        or you can use help followed by a command to get precise help on the command.\n\
",
                .callback = help,
        },
        [1] = (struct Command) {
                .name = "init",
                .desc = "initialize project [NAME]",
                .help =
"\
        the init command is here to create your projects.\n\
        you can use the init command followed by a name that respect this expression '[a-zA-Z0-9_]+'\n\
        and it will initialize a directory containing a include directory, a sources directory and a config file.\n\
",
                .callback = init,
        },
        [2] = (struct Command) {
                .name = "build",
                .desc = "build project",
                .help =
"\
        the build command is here to build your project.\n\
        you can use the build command alone to build your project using all .c file in the src directory.\n\
",
                .callback = build,
        },
        [3] = (struct Command) {
                .name = "run",
                .desc = "run project",
                .help =
"\
        the run command is here to build and run your project.\n\
        you can use the run command alone to build and run your project using all .c file in the src directory.\n\
",
                .callback = run,
        },
        [4] = (struct Command) {
                .name = "add",
                .desc = "add a library to your project",
                .help =
"\
        the add command is here to help you add libraries to your projects. \n\
        you can use the add command with libraries names and climb will try to add them to your project\n\
        you can see all the available libraries using the list command or by going to the official climb repository\n\
",
                .callback = NULL,
        },
        [5] = (struct Command) {
                .name = "list",
                .desc = "list all available libraries",
                .help = 
"\
        the list command is here to show you all the available libraries for your climb project.\n\
        you can use the list command alone to get a complete list of libraries,\n\
        or you can use it with a query to search for specific library.\n\
",
                .callback = list,
        },
};

int main(int c, char **v) {
        if(c < 2) {
                usage(v[0], "not enough arguments");
                return EXIT_FAILURE;
        }
        int ret = EXIT_SUCCESS;
        bool founded = false;
        for(int i = 0; i < ARR_LEN(COMMANDS); i++) {
                if(strcmp(COMMANDS[i].name, v[1]) == 0) {
                        founded = true;
                        ret = COMMANDS[i].callback(c - 1, v + 1);
                        break;
                }
        }
        if(founded) return ret;
        else {
                usage(v[0], "unknown command");
                return EXIT_FAILURE;
        }
}
