#ifndef _http_h_
#define _http_h_

#include <gnutls/gnutls.h>
#include <stdbool.h>

struct http_message {
	struct {
		int code;
		int length;
	} header;
	char *content;
	int length;
	struct {
		int in_content;
		int chunk;
		char buf[4096];
		int size;
		char *offset;
		char *last;
		int free;
		int left;
		int total;
	} state;
};

struct https_handler {
        gnutls_certificate_credentials_t cred;
        gnutls_session_t session;
        int fd;
        struct http_message msg;
        int len;
        bool setlen;
        bool valid;
};

struct http_url {
	char *protocol;
	char *host;
	char *query;
};

struct http_string {
        char buf[4096];
        int len;
};

extern char *https_err;
extern int https_code;

/* http function */
struct http_url *https_parse_url(const char *url);

/* https functions */
bool https_download_file(char *url, char *file);
char *https_get_code();

struct https_handler https_get_download_handler(char *url);
struct http_string https_read_partial(struct https_handler *hand, bool *valid, bool *end);
void https_destroy_handler(struct https_handler *hand);

#endif
