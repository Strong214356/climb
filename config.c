#include "climb_utils.h"

toml_table_t *getConfig() {
        if(!exist("climb.toml")) {
                printf("[ERROR] => directory is not a climb project\n");
                return NULL;
        }
        char errbuf[200];
        FILE *config = fopen("climb.toml", "r");
        toml_table_t *conf = toml_parse_file(config, errbuf, sizeof(errbuf));
        fclose(config);
        if(!conf) {
                printf("[ERROR] => unable to parse config: %s\n", errbuf);
                return NULL;
        }
        return conf;
}
